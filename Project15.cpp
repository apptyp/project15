﻿
#include <iostream>

void FindOddNumbers(int N, bool IsOdd)
{
	if (IsOdd == true)
	{
		for (int i = 0; i < N; i++)
		{
			if (i % 2 != 0)
			{
				std::cout << i << "\n";
			}
		}
	}
	else
	{
		for (int i = 0; i < N && i % 2 == 0; i += 2)
		{
			std::cout << i << "\n";
		}
	}
}

int main()
{
	FindOddNumbers(25, true);
}
